FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

# the 2.3 branch won't work with php 7.2 (https://github.com/CachetHQ/Cachet/issues/3719)
ARG VERSION=c92cc430428975cb764808901eda7790e804bb7e

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN wget https://github.com/cachethq/Cachet/archive/${VERSION}.tar.gz -O -| tar -xz -C /app/code --strip-components=1 && \
    chown -R www-data:www-data /app/code

RUN sudo -u www-data composer install --no-interaction --prefer-dist --no-suggest --optimize-autoloader --no-dev && \
    sudo -u www-data composer clear-cache

RUN chmod -R g+rw /app/code/storage \
    && mv /app/code/storage /app/code/storage.template && ln -s /app/data/storage /app/code/storage \
    && mv /app/code/bootstrap/cache /app/code/bootstrap/cache.template && ln -s /run/cachethq/bootstrap-cache /app/code/bootstrap/cache \
    && rm -rf /app/code/database/backups && ln -s /app/data/database-backups /app/code/database/backups \
    && ln -s /app/data/env /app/code/.env

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/cachethq.conf /etc/apache2/sites-enabled/cachehq.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod rewrite

# Note that the sessions stuff is unused because cachet uses lavarel sessions (storage/framework/sessions)
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 25M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 25M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 64M

ADD env.production start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
